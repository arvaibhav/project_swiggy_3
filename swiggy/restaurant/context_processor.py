from core.models import Category

def get_categories(request):
    categories=Category.objects.all()
    categories =[ (x.pk,x.category_name) for x in categories]
    return {
        'categories':categories
    }


