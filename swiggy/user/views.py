from django.shortcuts import render, redirect
from django.contrib.gis.geos import Point
from django.views.decorators.csrf import csrf_exempt
import json, redis
from swiggy.settings import GEOPOSITION_GOOGLE_MAPS_API_KEY, redis_store
from django.http import HttpResponse, JsonResponse
from core.models import *
import requests
from django.contrib.auth.decorators import login_required
from django.contrib.gis.geos import GEOSGeometry
from pprint import pprint
from django.contrib.gis.measure import Distance


# base_url = "https://developers.zomato.com/api/v2.1/"


def locate(typeofuser, location):
    print(typeofuser)
    locations = []
    print(location)
    users = User.objects.filter(user_type=typeofuser)
    radius = 1234444
    degrees = radius / 111.325
    places = Location.objects.filter(location__dwithin=(location, degrees))
    print(places[0], "jhk")
    for user in users:
        restaurant_location = Location.objects.get(user=user)

        point1 = location
        point2 = restaurant_location.location
        distance = point2.distance(point1) * 100
        if distance < 1000000000000000:
            if typeofuser == 'restaurant':
                locations.append(user.restaurant)
            else:
                locations.append(user)
    return locations


@login_required
@csrf_exempt
def location(request):
    user_location = Location.objects.get(user=request.user)
    locations=Restaurant.objects.all()
    try:
        locations = locate('restaurant', user_location.location)
    except:
        pass

    return render(request, 'user_restaurants.html', {'restaurants': locations, })


def restaurant(request, pk):
    redis_store.set(request.user.id, pk)
    restarnt = Restaurant.objects.get(pk=pk)
    food_items = FoodItem.objects.filter(restaurant=restarnt)
    return render(request, 'restaurant.html', {'items': food_items, 'restaurant': restarnt})


@login_required
def restaurant(request, pk):
    redis_store.set(request.user.id, pk)
    restarnt = Restaurant.objects.get(pk=pk)
    food_items = FoodItem.objects.filter(restaurant=restarnt)
    return render(request, 'restaurant.html', {'items': food_items, 'restaurant': restarnt})


@login_required
@csrf_exempt
def checkout(request):
    if request.method == 'POST':
        json_data = json.loads(request.body.decode("utf-8"))
        restaurant = Restaurant.objects.get(pk=int(redis_store.get(request.user.id)))
        status = OrderStatus()
        status.save()
        user_order = Order(customer=request.user, restaurant=restaurant, status=status)
        total_cost = 0
        for data in json_data:
            item, quantity = data['name'], data['quantity']
            print(item, quantity)
            food_item = FoodItem.objects.filter(restaurant=restaurant).filter(name=item).first()
            total_cost += food_item.cost * quantity
            user_order.total_cost = total_cost
            user_order.save()
            order_item = OrderedItem(food=food_item, order=user_order, quantity=quantity)
            order_item.save()
        redis_store.set(request.user.id, user_order.id)
        return HttpResponse("Success")
    else:
        orders = []
        total_cost = 0
        order_item = int(redis_store.get(request.user.id))
        items = OrderedItem.objects.filter(order=order_item)
        for item in items:
            orders.append({'name': item.food.name, 'cost': item.food.cost, 'quantity': item.quantity})
            total_cost += item.food.cost * item.quantity
        return render(request, 'checkout.html', {'orders': orders, 'cost': total_cost})


@login_required
def status(request):
    order_id = int(redis_store.get(request.user.id))
    order = Order.objects.get(pk=order_id)
    user_location = Location.objects.get(user=request.user)
    delivery_guy =User.objects.filter(user_type='delivery_guy').first()
    try:
        delivery_guy = locate('delivery_guy', user_location.location)[0]
    except:
        pass
    order.delivery_guy = delivery_guy
    order.save()
    return redirect('orderstatus', pk=order_id)


@csrf_exempt
def order_status(request, pk):
    order = Order.objects.get(pk=pk)
    if request.method == 'POST':
        if (order.status.order_prepared and order.status.delivery_guy_assigned and order.status.order_delivered):
            return JsonResponse({'status': 3})
        elif (order.status.order_prepared and order.status.delivery_guy_assigned):
            return JsonResponse({'status': 2})
        elif (order.status.order_prepared):
            return JsonResponse({'status': 1})
        else:
            return JsonResponse({'status': 0})
    else:
        return render(request, 'status.html', {'pk': pk})
